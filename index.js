"use strict"
// Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript
// Ассинхронность в моём понимании возможность языка ожидать выполнение действий другой функции и после чего выполнять свои действия

let btn = document.querySelector('.getip-btn')
class Request {
    async getRequest(URL) {
        try {
            const response = await fetch(URL)
            return await response.json()
        } catch(err) {
            console.log(err);
        }
    }
}

class Post{
    async getIp(){
        let request = new Request()
        let ip = await request.getRequest("https://api.ipify.org/?format=json")
        return ip
    }
    getInfoIp(){
        return this.getIp().then(ip =>{
            let ipUser = Object.values(ip)
            let request = new Request()
            return request.getRequest(`http://ip-api.com/json/${ipUser}?fields=continent,country,regionName,city`, {
            }).then(data =>{
                return data
            })
        })
    }
    render(data){
        const {continent, country, regionName, city} = data;
        const list = document.createElement("ul")
        list.classList.add("list-group")
        list.insertAdjacentHTML("beforeend", `
        <li class="list-group-item">Continent: ${continent}</li>
        <li class="list-group-item">country: ${country}</li>
        <li class="list-group-item">regionName: ${regionName}</li>
        <li class="list-group-item">city: ${city}</li>
    `)
            return list
    }
    btnEvent(){
        btn.addEventListener('click', async e =>{
            this.getInfoIp().then(data =>{
                btn.after(this.render(data))
            })
        })
    }

}
let post = new Post
post.btnEvent()